using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed = 5f;
    public Rigidbody playerRigidbody;
    private bool Alive = true;

    [SerializeField] float jumpForce = 400f;
    [SerializeField] LayerMask groundMask; 

    private void FixedUpdate()
    {
        if (!Alive) return;

        Vector3 forwardMove = transform.forward * moveSpeed * Time.fixedDeltaTime;
        playerRigidbody.MovePosition(playerRigidbody.position + forwardMove);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }
    }

    void Jump()
    {
        float height = GetComponent<Collider>().bounds.size.y;
        bool isGrounded = Physics.Raycast(transform.position, Vector3.down, (height / 2) + 0.1f, groundMask);

        playerRigidbody.AddForce(Vector3.up * jumpForce);
    }

    public void Die()
    {
        Alive = false;

        Invoke("Restart", 1);
    }

    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
