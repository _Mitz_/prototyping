using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PrototypeTwo
{
    public class PlatformTile : MonoBehaviour
    {
        public Transform startPoint;
        public Transform endPoint;
        public GameObject[] Obstacles;

        public void ActivateRandomObstacles()
        {
            DeactivateAllObstacles();

            System.Random random = new System.Random();
            int randomNumber = random.Next(0, Obstacles.Length);
            Obstacles[randomNumber].SetActive(true);
        }

        public void DeactivateAllObstacles()
        {
            for (int i = 0; i < Obstacles.Length; i++)
            {
                Obstacles[i].SetActive(false);
            }
        }
    }
}
