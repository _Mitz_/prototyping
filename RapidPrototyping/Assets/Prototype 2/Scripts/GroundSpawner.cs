using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundSpawner : MonoBehaviour
{
    public GameObject groundTiles;
    Vector3 newSpawnLocation;

    public void SpawnTile()
    {
        GameObject temp = Instantiate(groundTiles, newSpawnLocation, Quaternion.identity);
        newSpawnLocation = temp.transform.GetChild(1).transform.position;
    }

    private void Start()
    {
        for (int i = 0; i < 15; i++)
        {
            SpawnTile();
        }
    }

}
