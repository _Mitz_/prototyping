using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using PrototypeTwo;

public class GroundGenerator : MonoBehaviour
{
    public Camera mainCamera;
    public Transform startPoint;
    public PlatformTile tilePrefab;
    public float moveSpeed = 12f;
    public int tilesToSpawn = 15;
    public int normalTiles = 3;

    List<PlatformTile> spawnedTiles = new List<PlatformTile>();
    //int nextTile = -1;
    [HideInInspector]
    public bool gameOver = false;
    static bool gameStart = false;
    float score = 0;

    public static GroundGenerator Instance;

    void start()
    {
        Instance= this;

        Vector3 spawnPosition = startPoint.position;
        int tilesWithoutObstacles = normalTiles;
        for (int i = 0; i < tilesToSpawn; i++)
        {
            spawnPosition -= tilePrefab.startPoint.localPosition;
            PlatformTile spawnedTile = Instantiate(tilePrefab, spawnPosition, Quaternion.identity) as PlatformTile;
            if(tilesWithoutObstacles > 0)
            {
                spawnedTile.DeactivateAllObstacles();
                tilesWithoutObstacles--;
            }
            else
            {
                spawnedTile.ActivateRandomObstacles();
            }

            spawnPosition = spawnedTile.endPoint.position;
            spawnedTile.transform.SetParent(transform);
            spawnedTiles.Add(spawnedTile);
        }
    }

    void Update()
    {
        if(!gameOver && gameStart)
        {
            transform.Translate(-spawnedTiles[0].transform.forward * Time.deltaTime * (moveSpeed + (score/500)), Space.World);
            score += Time.deltaTime * moveSpeed;
        }

        if (mainCamera.WorldToViewportPoint(spawnedTiles[0].endPoint.position).z < 0)
        {
            PlatformTile tile = spawnedTiles[0];
            spawnedTiles.RemoveAt(0);
            tile.transform.position = spawnedTiles[spawnedTiles.Count - 1].endPoint.position - tile.startPoint.localPosition;
            tile.ActivateRandomObstacles();
            spawnedTiles.Add(tile);
        }

        if (gameOver || !gameStart)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (gameOver)
                {
                    Scene scene = SceneManager.GetActiveScene();
                    SceneManager.LoadScene(scene.name);
                }
                else
                {
                    gameStart = true;
                }
            }
        }
    }

    void OnGUI()
    {
        if (gameOver)
        {
            GUI.color = Color.red;
            GUI.Label(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 100, 200, 200), "Game Over\nYour Score is: " + ((int)score) + "\nPress 'Space' to Restart");
        }
        else
        {
            if (!gameStart)
            {
                GUI.color = Color.red;
                GUI.Label(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 100, 200, 200), "Press 'Space' to Start");
            }
        }

        GUI.color = Color.green;
        GUI.Label(new Rect(5, 5, 200, 25), "Score: " + ((int)score));
    }
}
