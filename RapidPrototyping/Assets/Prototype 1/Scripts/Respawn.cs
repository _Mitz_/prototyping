using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PrototypeOne;

public class Respawn : MonoBehaviour
{
    private Transform spawnPoint1;
    private Transform spawnPoint2;

    private Transform playerPosition1;
    private Transform playerPosition2;

    void Start()
    {
        playerPosition1 = GameObject.FindGameObjectWithTag("Player1").transform;
        playerPosition2 = GameObject.FindGameObjectWithTag("Player2").transform;
        spawnPoint1 = GameObject.FindGameObjectWithTag("Respawn1").transform;
        spawnPoint2 = GameObject.FindGameObjectWithTag("Respawn2").transform;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player1")
        {
            playerPosition1.position = new Vector3(spawnPoint1.position.x, spawnPoint1.position.y, spawnPoint1.position.z);
            playerPosition1.rotation = Quaternion.Euler(0, 0, 0);
        }

        if (other.gameObject.tag == "Player2")
        {
            playerPosition2.position = new Vector3(spawnPoint2.position.x, spawnPoint2.position.y, spawnPoint2.position.z);
            playerPosition2.rotation = Quaternion.Euler(0, 0, 0);
        }
    }
}
