using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace PrototypeOne
{
    public class CameraController : MonoBehaviour
    {
        public GameObject playerCharacter;
        private Vector3 cameraOffset = new Vector3(0, 4, -10);
        void LateUpdate()
        {
            transform.position = playerCharacter.transform.position + cameraOffset;
        }
    }
}

