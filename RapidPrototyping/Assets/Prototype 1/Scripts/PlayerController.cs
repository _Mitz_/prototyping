using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static PlayerController;
using PrototypeOne;

public class PlayerController : MonoBehaviour
{ 
    public enum PlayerControls { WASD, Arrows }
    public PlayerControls playerControls = PlayerControls.WASD;

    private float moveSpeed = 20.0f;
    private float turnSpeed = 45.0f;
    public float dashSpeed = 25.0f;

    private float horizontalInput;
    private float forwardInput;

    public Rigidbody player;
    public float dashTime = 0.5f;
    
    void Update()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        forwardInput = Input.GetAxis("Vertical");

        if((playerControls == PlayerControls.WASD && Input.GetKey(KeyCode.W)) || (playerControls == PlayerControls.Arrows && Input.GetKey(KeyCode.UpArrow)))
        {
            transform.Translate(Vector3.forward * Time.deltaTime * moveSpeed * forwardInput);
        }
        else if ((playerControls == PlayerControls.WASD && Input.GetKey(KeyCode.S)) || (playerControls == PlayerControls.Arrows && Input.GetKey(KeyCode.DownArrow)))
        {
            transform.Translate(Vector3.forward * Time.deltaTime * moveSpeed * forwardInput);
        }

        if ((playerControls == PlayerControls.WASD && Input.GetKey(KeyCode.A)) || (playerControls == PlayerControls.Arrows && Input.GetKey(KeyCode.LeftArrow)))
        {
            transform.Rotate(Vector3.up, turnSpeed * horizontalInput * Time.deltaTime);
        }
        else if ((playerControls == PlayerControls.WASD && Input.GetKey(KeyCode.D)) || (playerControls == PlayerControls.Arrows && Input.GetKey(KeyCode.RightArrow)))
        {
            transform.Rotate(Vector3.up, turnSpeed * horizontalInput * Time.deltaTime);
        }

        if ((playerControls == PlayerControls.WASD && Input.GetKeyDown(KeyCode.LeftShift)) || (playerControls == PlayerControls.Arrows && Input.GetKeyDown(KeyCode.RightShift)))
        {
            StartCoroutine(Dash());
        }
    }

    IEnumerator Dash()
    {
        float startTime = Time.time;

        while (Time.time < startTime + dashTime)
        {
            transform.Translate(Vector3.forward * Time.deltaTime * dashSpeed * forwardInput);

            yield return null;
        }
    }
}
